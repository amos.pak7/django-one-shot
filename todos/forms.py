from django.forms import ModelForm
from todos.models import TodoList, TodoItem


class CreateTodoForm(ModelForm):
    class Meta:
        model = TodoList
        fields = [
            "name",
        ]


class UpdateTodoForm(ModelForm):
    class Meta:
        model = TodoList
        fields = [
            "name",
        ]


class TodoItemForm(ModelForm):
    class Meta:
        model = TodoItem
        fields = [
            "task",
            "due_date",
            "is_completed",
            "list",
        ]

    def __str__(self):
        return self.tasks
