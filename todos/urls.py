from django.urls import path
from . import views

urlpatterns = [
    path("items/create/", views.todo_item_create, name="todo_item_create"),
    path("<int:id>/delete/", views.todo_list_delete, name="todo_list_delete"),
    path("<int:id>/edit/", views.todo_list_update, name="todo_list_update"),
    path("create/", views.todo_list_create, name="todo_list_create"),
    path("<int:id>/", views.todo_list_detail, name="todo_list_detail"),
    path("", views.todo_list_list, name="todo_list_list"),
]
