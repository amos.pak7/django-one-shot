from django.shortcuts import render, get_object_or_404, redirect
from .models import TodoList, TodoItem
from todos.forms import CreateTodoForm, UpdateTodoForm, TodoItemForm


def todo_list_list(request):
    todo_lists = TodoList.objects.all()
    context = {"todo_lists": todo_lists}
    return render(request, "todolist/list.html", context)


def todo_list_detail(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    tasks = TodoItem.objects.filter(list=todo_list)
    context = {"todo_list": todo_list, "tasks": tasks}
    return render(request, "todolist/detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = CreateTodoForm(request.POST)
        if form.is_valid():
            todo_list = form.save()
            return redirect("todo_list_detail", id=todo_list.id)
    else:
        form = CreateTodoForm()

    context = {"form": form}
    return render(request, "todolist/create.html", context)


def todo_list_update(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = UpdateTodoForm(request.POST, instance=todo_list)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=id)
    else:
        form = UpdateTodoForm(instance=todo_list)

    context = {"form": form}
    return render(request, "todolist/update.html", context)


def todo_list_delete(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        todo_list.delete()
        return redirect("todo_list_list")

    context = {"todo_list": todo_list}
    return render(request, "todolist/delete.html", context)


def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = TodoItemForm()

    context = {"form": form}
    return render(request, "todolist/todoitem.html", context)


# Create your views here.
